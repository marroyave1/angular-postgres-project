package com.personal.postgresql.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
@Data
@Entity
@Table(name="PAGO")
public class Pago implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String nombre;
	private int valor;
	
	@ManyToOne
	@JoinColumn(name="tipo_transaccion")
	private MaestroValor tipoTransaccion;
	
	@ManyToOne
	@JoinColumn(name="id_estacion")
	private Estacion idEstacion;
	
	@ManyToOne
	@JoinColumn(name="id_qr")
	private Qr idQr;
	
}
