package com.personal.postgresql.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GeneratorType;

import lombok.Data;
@Data
@Entity
@Table(name = "USUARIO")
public class Usuario implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private String nombres;
	
	private String apellidos;
	
	private String documento;

	@Column(unique = true)
	private String email;
	
	private String telefono;
	
	@Column(unique = true)
	private String contraseña;
	
	private String direccion;

	@Column(name = "fecha_nacimiento")
	private Date fechaNacimiento;
	
	@JoinColumn(name="tipo_sangre")
	@ManyToOne
	private MaestroValor tipoSangre;
	
	@JoinColumn(name="tipo_documento")
	@ManyToOne
	private MaestroValor tipoDocumento;
	
	@JoinColumn(name="datos_biometricos")
	@ManyToOne
	private DatosBiometricos datosBiometricos;
	
	@JoinColumn(name="cuenta_usuario")
	@ManyToOne
	private Cuenta cuentaUsuario;
	
	@JoinColumn(name="id_estado_cuenta")
	@ManyToOne
	private EstadoCuenta estadoCuenta;

}
